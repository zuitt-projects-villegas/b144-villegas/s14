/*writing Comment in javascript*/
//one-line comments ctrl +/ 
/* multi-line comment - ctrl + / */
console.log("Hello Batch 144!");

/*
	javascript -  we can see or log messages in our console.

	Browser Consoles are part of our browsers which will allow us to see/log messages, data or information from our programming language - Javascript.

	For most browers, consoles are easily accessed through its developer tools in the console tab.

	In facts, consoles is browsers will allow us to add some Javascript expressions.
	
	Statements

	Statements are instructions, expressions that we add to our proramming language to communicate with our computers.

	JS Statements usually ends in a semi colon (;) However, JS has implemented a way tp automatically add semicolons at the end of a line/statements. Semi Colons can actuallt be ommited in creating JS statements but Semicolons in JS are added to mark the end of the statement.

	Syntax

	Syntax in programming is a set of rules that descrives how statements are properly made/contructed.

	Lines/blocks of code must follow a certain of rules for it to work properly.

*/
console.log("Ian Clark Villegas");
/*
	Mini-Activity


*/

/*console.log("Chicken Curry");
console.log("Chicken Curry");
console.log("Chicken Curry");*/

let food1 = "Chicken Curry";
console.log(food1);

/*
	Variables are a way to store information or data within our JS.

	To create a variable we first declare the name of the variable with either the let/const keyword:

	let nameOfVariable

	Then, we can initialize the variable with a value or data.

	let nameOfVariable = <data>;
*/

console.log("My favorite food is: " + food1);

let food2 = "Sisig"

console.log("My favorite food are: " + food1 + " and " + food2);

let food3;

console.log(food3);

/*
	We can create variables without add an initial value, however, the variable's content is undefined.

*/

food3 = "Chickenjoy";
/*
	We can update the content of a let variable by reassigning the content using an assignment operator (=) 

	assignment operator (=) = lets us assign data to a variable.
*/

console.log(food3);

/*
	Mini Activity:

	Reassign variable food1 with new data, with another of your favorite food.

	Reassign variable food2 with new data, with another of your favorite food.
	
*/

food1 = "Sinigang";
food2 = "Adobo";

console.log(food1);
console.log(food2);

/*
	We can update our variables with an assignment operator withour needing to use the let keyword again.

	We cannot create another variable with the same name. It will result in an error.

*/

// const keyword
/*
	const keyword will also allow us to create variables. However, wth a const keyword we create constant variables, which means there data to saved in a constant will not change, cannot be changed and should not be changed.
*/

const pi = 3.1416;

console.log(pi);

// Trying to re-assign a const variable will result into an error

/*pi = "pizza"

console.log(pi)*/

/*const gravity;

console.log(gravity);*/

// We also cannot declare/create a const variable without initialization or an initial value.

/*
	Mini--Activity

	Create a let variable called myName with out an initial value.

	Create a const variable called sunriseDirection with "East" as value

	Create a const variable called sunsetDirection with "West" as value
	
	Log all three variables in the console

*/

let myName;

const sunriseDirection = "East";
const sunsetDirection = "West";

/*You can actually log multiple items, variables, data in console.log separated by ,*/

console.log(myName,sunriseDirection,sunsetDirection);

/*
	Guides in creating a JS variable:

	1. We can crate a let variable with the let keyword. let varibales can be reassigned but not redeclared.

	2. Creating a variable has two parts: Declaration of the variable name and Initialization of the initial value of the variable using an assignment operator (=)

	3. A let variable can be declared without initialization. However the value of the variable will be undefined until it iis re-assigned with a value.

	4. Not Defined vs Undefined. Not Defined errors means the variable is used but NOT declared. Undefined results from a variable that is used but not initialized

	5. We can use Const keyword to create constant variables. Constant variables cannot be declared without initialization.
	Constant variables cannot be re-assigned.

	6. When creating variable names, start with small caps thiss i sbecause of avoiding conflict with syntax in JS that is named with capital letters.
	
	7. If the variable would need two words, the naming convention is the use of camelCase. Do not add a space for variables with words as names. 
*/

/*
	strings are date which are alphanumerical text. It could be a name, a phrae or even a sentence. We can create string with single quote ('') or with double ("").
	
	(strings are data)
	(variables are store data's)

	variable = 'string data'
*/

console.log("Sample String Data");

let country = "Philippines";
let province = "Rizal";

console.log(province,country);

/*
	Concatentation - is a way for us to add strings to together and have a single string. We can use out "+" symbol for this.
*/

let fullAddress = province + ',' + country;

console.log(fullAddress);

let greating = "I live in" + country;

console.log(greating);

/*In JS, When you use the + sign with strings we have concatenation.*/

let numString = "50";
let numString2 = "25";

console.log(numString + numString2);

/*	Strings have property called .length and it tells us the number of characters in a string.

	Spaces, Commas, Periods can also be characters when included in a string.

	It will return a number type dara.

*/

let hero = "Captain America";

console.log(hero.length);

/*Number Type*/
/*
	Number Type data can actually used in proper mathematical equations and operations.

*/
let students = 16;
console.log(students);

let num1 = 50;
let num2 = 25;

/*
	Addition Operator will allow us to add two number types and return the sum as a nuber data type.

	Operations return a value and that value can be saved in a variable.

	What if we use the addition operator on a number type and a numerical string? It results to concatenation.

	parseInt() will allow us to turn anumeric string in a proper number type.
*/

let sum1 = num1 + num2


console.log(num1+num2);

let numString3 = "100";

console.log(num1+numString3);

let sum2 = parseInt(numString3) + num1

console.log(sum2);

/*
	Mini-Activity

	Save the sum of the variables sum1 and sum2 in a variable called sum3

	Save the sum of the variables numstring2 and num2 in a variable called sum4.
*/

let sum3 = sum1 +sum2;

let sum4 = parseInt(numString2) + num2;

console.log(sum3,sum4);

/*
	Subtraction operator

	Will let us get the Difference between two number types. It will also result to a proper number type.

	When subtraction operator is used on a string and a number, the string will be converted into a number automatically and then JS will performthe operation.

	This automatic conversion from one type to another is called type Conversion or Type Coercion or Forced Coercion.

	When a text string is subtracted with a number, it will wresult in NAN or Not a number because when JS converted the text string it results to NaN and NaN-number = Nan
*/

let difference = num1-num2;
console.log(difference);

let difference2 = numString3 - num2;
console.log(difference2);

let difference3 = hero - num2;
console.log(difference3)

let string1 = "fifteen";

console.log(num2 - string1);

/*Multiplication Operator (*)*/

let num3 = 10;
let num4 = 5;

let product = num3*num4;
console.log(product);
let product2 = numString3 * num3;
console.log(product2);
let product3 = numString * numString3;
console.log(product3);

/*Division Operator (/)*/
let num5 = 30;
let num6 = 3;
let quotient = num5/num6;
console.log(quotient);
let quotient2 = numString3/5;
console.log(quotient2);
let quotient3 = 450/num4;
console.log(quotient3);

/*Boolean (true or false)*/
/*Boolean is usally used for logical operations or for if-else conditions.*/
/*Naming convention for a variable that boolean is a yes or no question.*/

let isAdmin = true;
let isMarried = false;

/*	
	You can actually name your variable any way you want however as the best practice it should be:


	1. appropriate.
	2. definitive of the value it contains
	3. semantically correct.

	let pikachu = "ian clark villegas"
	console.log(pikachu);

*/

// Arrays 
// Arrays are a special kind of data type wherein we can store multiple values.
// an array can store multiple values and even of different types. for best practice, keep the data type of items in an array uniform.
// Values in array are separated by comma. Failing to do so, will result in an error.
// Ann array is created with an Array Litera ([])

	let koponanNiEugene = ["Eugene","Alfred","Dennis","Vincent"];
	console.log(koponanNiEugene);
// Array indices are markers of the order of the items in the array. Array index starts at 0
	// To access an array item: arrayName[index]
	// Bad Practice for an Array
	console.log(koponanNiEugene[0]);
	let array2 = ["One Punch Man",true,500,"Saitama"]
	console.log(array2);

// Objects 
/*
	Objects are another kind of special data type used to mmimic real world objects. 
	With this we can add information that makes sense, thematically relevent, and of different data types. 

	Object can be created with Object Literals ({})

	Each value is given label which makes data significant and meaningful this pairing of data and "label" is what we call a Key-Value pair.

	A key-value pair together is called a a property.

	each property is separated by a comma.
*/

	let person1 = {
		heroName: "One Puch Man",
		isRegistered: true,
		salary: 500,
		realName: "Saitama"
	};
	// To get the value of an object's property, we can access it using dot notation.
	// objectName.propertyName
	console.log(person1.realName);

	/*
		Mini-Activity

			Create a variable with a group of data/values with the same type.
				-The group of data/values should contain the names of your favorite bands.
				-variable name: myFavoriteBands
			Create a variables which can contain multiple values of differing types and describes a single person.
				-This data type should be able to contain a multiple key value pairs
				firstName: 

	*/
let myFavoriteBands = ["Parokya ni Edgar","One Direction","Kamikazee","The Script"]
console.log(myFavoriteBands);
let me = {
	firstName: "Ian Clark",
	lastName: "Villegas",
	isWebDeveloper: true,
	hasPortfolio: true,
	age: 24,
	/*Properties of an object should relate to each other and thematically relevant to describe a single item/object*/
	brand: "Toyota"
};
console.log(me);

/*undefined vs Null*/

/*Null*/

/*Is the explicit declaration that there is no value.*/

let sampleNull = null;

/*Undefined*/
/*Means that the variable exists howver a value was not initialized with the variable.*/

let undefinedSample;
console.log(undefinedSample);

// Certain processes in programming explicitly returns null to indicate that the task resulted to nothing.

let foundResult = null;

// for undefined, this normally caused by developers creating variables that have no value or data associated with them.
// The variable does exist but its value is still unknown.

let person2 = {
	name: "Patricia"
	age: 28
};
// because the variabe person2 does exist however the property is.Admin does not
console.log(person2.isAdmin);