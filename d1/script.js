// Functions



function printStar(){
	console.log("*")
	console.log("**")
	console.log("***")
};
printStar();

function sayHello(name){
	console.log('Hello' + name)
}
sayHello(6);

// a function can call a function inside

function alertPrint() {
	alert("Hello");
	console.log("Hello")
}
alertPrint();	

	// Functions that accepts two numbers and print the sum

	// Parameters

/*function addSum(x,y){
	let sum = x + y
	console.log(sum)
}

addSum(13,2) // arguments
addSum(23,56)*/

// function with 3 parameters
// String Template Literals
// Interpolation
function printBio(lname,fname,age){
	console.log(`Hello Mr. ${lname} ${fname} ${age}`)
}

printBio('Stark','Tony',50)

// Return keyword

function addSum(x,y){
	return y-x
	console.log(x+y)
}
// addSum(3,4)
let sumNum = addSum(3,4)

